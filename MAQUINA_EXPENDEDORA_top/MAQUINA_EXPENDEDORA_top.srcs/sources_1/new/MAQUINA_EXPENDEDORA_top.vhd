----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.12.2021 09:45:02
-- Design Name: 
-- Module Name: MAQUINA_EXPENDEDORA_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAQUINA_EXPENDEDORA_top is
  Port (
        -- Entradas
        prod: IN std_logic_vector (3 DOWNTO 0);
        CLK: IN std_logic ;
        DIEZ_cent: IN std_logic;
        VEINTE_cent: IN std_logic;
        CINCUENTA_cent: IN std_logic;
        UNO_euro: IN std_logic_vector;
        SENSOR_producto: IN std_logic;
        SENSOR_monedas: IN std_logic;
        RESET_FSM: IN std_logic;
        
        --Salidas
        display_segmentos: out std_logic_vector(7 downto 0);
        display_eleccion: out std_logic_vector(2 downto 0);
        expulsar: out std_logic_Vector(3 downto 0);
        monedas_fuera: out std_logic
  );
end MAQUINA_EXPENDEDORA_top;

architecture Behavioral of MAQUINA_EXPENDEDORA_top is

--------------------------------------------------------------------------------------
-------------------------------Senyales intermedias ----------------------------------------------------------------------
   
    --Sincronizador->EDGE_DETECTOR
    signal sync1: std_logic;
    signal sync2: std_logic;
    signal sync3: std_logic;
    signal sync4: std_logic;
    
    --EDGE_DETECTOR->COMPROBAR CANTIDAD
    signal edege1: std_logic;
    signal edege2: std_logic;
    signal edege3: std_logic;
    signal edege4: std_logic;
    
    --Acumulador->COMPPROBAR CANTIDAD
    signal valor_total: std_logic_vector (7 downto 0);
    
    --COMPROBAR CANTIDAD->Expulsar signal
    signal p: std_logic_vector (3 downto 0);
    signal error1: std_logic;
    
    --ACUMULADOR<->FSM
    signal hay_moneda: std_logic;
    signal acumular: std_logic;
    
    --COMPROBAR CANTIDAD <-> FSM 
    signal producto_elegido: std_logic;
    signal cantidad_mayor: std_logic;
    signal cantidad_correcta: std_logic;
    signal cantidad_menor: std_logic;
    signal comprobar: std_logic;
    
    --EXPULSAR PRODUCTO <-> FSM 
    signal expulsar_producto: std_logic;
    signal producto_expulsado: std_logic;
    
    --EXPULSAR MONEDAS <-> FSM 
    signal expulsar_monedas_fsm: std_logic;
    signal no_hay_monedas: std_logic;
    
    --RESET
    signal reset_entidades: std_logic_vector (4 downto 0);
    
----------------------------------------------------------------------------------------------------------------------    
---------------------------------Entidades-----------------------------------------------------------------------------------------------------------
COMPONENT EDGEDTCTR
    generic (
        CYCLES: positive := 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    );
    port (
        CLK : in std_logic;      -- Se�al de reloj
        SYNC_IN : in std_logic;  -- Entrada sincronizada
        EDGE : out std_logic	 -- Flanco detectado
    );
END component;

component  SYNCHRNZR is
    port (
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
end COMPONENT;

component  fsm is
port (
    RESET : in std_logic;
    CLK : in std_logic;
    MONEDA_IN: in std_logic;
    PROD_ELEGIDO: in std_logic;
    DINERO_MAYOR: in std_logic;
    DINERO_MENOR: in std_logic;
    DINERO_IGUAL: in std_logic;
    PROD_EXPULSADO: in std_logic;
    MONEDAS_EXP: in std_logic;
    ENABLE: out std_logic_vector(4 downto 0);
    RESET_ENTITIES: out std_logic_vector(4 downto 0) -- Se�ales de reset para las entidades
);
end component ;  

component EXPULSAR_MONEDAS_ENTITY is
    Port ( sensor_monedas : in STD_LOGIC;
           expulsar_monedas : in STD_LOGIC;
           no_hay_monedas: out STD_LOGIC;
           monedas_fuera : out STD_LOGIC);
end component;

component  EXPULSAR_PRODUCTO_ENTITY is
    Generic (cantidad: integer:= 4);
    Port ( producto : in STD_LOGIC_VECTOR (cantidad-1 downto 0);
           expulsar_producto : in STD_LOGIC;
           producto_expulsado : out STD_LOGIC;
           sensor_producto: in STD_LOGIC;
           expulsar : out STD_LOGIC_VECTOR (cantidad-1 downto 0));
end component;

---------------------------------------------------------------------
------------------------- Asignacion de senyales -------------------------------------------
begin

--Sincronizadores botones
Boton_sync_1: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(3),
        SYNC_OUT => sync1
    );
Boton_sync_2: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(2),
        SYNC_OUT => sync2
    );
    
Boton_sync_3: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(1),
        SYNC_OUT => sync3
    );

Boton_sync_4: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(0),
        SYNC_OUT => sync4
    );

--Detector de flancos
 Boton_edge_1: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync1,  -- Entrada sincronizada
        EDGE => edege1	 -- Flanco detectado
    );

 Boton_edge_2: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync2,  -- Entrada sincronizada
        EDGE => edege2	 -- Flanco detectado
    );
 
Boton_edge_3: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync3,  -- Entrada sincronizada
        EDGE => edege3	 -- Flanco detectado
    );

 Boton_edge_4: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync4,  -- Entrada sincronizada
        EDGE => edege4	 -- Flanco detectado
    );

-- Maquina de estados (FSM)
MaquinaEstados: fsm 
port map(
    RESET => RESET_FSM,
    CLK => clk,
    MONEDA_IN => hay_moneda,
    PROD_ELEGIDO => producto_elegido,
    DINERO_MAYOR =>cantidad_mayor,
    DINERO_MENOR => cantidad_menor,
    DINERO_IGUAL => cantidad_correcta,
    PROD_EXPULSADO => producto_expulsado,
    MONEDAS_EXP => no_hay_monedas,
    ENABLE(1)=> acumular,
    ENABLE(2)=> comprobar,
    ENABLE(3)=> expulsar_producto,
    ENABLE(4)=> expulsar_monedas_fsm,
    RESET_ENTITIES => reset_entidades -- Se�ales de reset para las entidadesRESET_FSM
);  

-- Comprobar cantidad y seleccion de producto
CantidadProducto: cantidadyproducto 
generic map(width => 4)
	port map (
    reset =>  reset_entidades(2),
	valortotal => valor_total,
	boton(3) => edege1,
	boton(2) => edeg
	comprobar: in std_logic;
	producto: out std_logic_vector (width-1 downto 0);
	senal: out std_logic_vector (0 to 2) -- segun el bit a 1 indicar� distintas cosas (0)=igual_monedas; (1)=menos_monedas; (2)=mas_monedas
);

-- signal valor_total: std_logic_vector (7 downto 0);
--signal p: std_logic_vector (3 downto 0);
  --  signal error1: std_logic;

-- Expulsar monedas
ExpulsarMonedas: EXPULSAR_MONEDAS_ENTITY
    Port map( sensor_monedas=> SENSOR_monedas,
           expulsar_monedas => expulsar_monedas_fsm,
           no_hay_monedas => no_hay_monedas,
           monedas_fuera => monedas_fuera
);

-- Expulsar producto elegido
ExpulsarProducto:  EXPULSAR_PRODUCTO_ENTITY 
    Generic map (cantidad => 4)
    Port map( producto => p,
           expulsar_producto => expulsar_producto,
           producto_expulsado => producto_expulsado,
           sensor_producto => SENSOR_producto,
           expulsar => expulsar
    );

end Behavioral;
