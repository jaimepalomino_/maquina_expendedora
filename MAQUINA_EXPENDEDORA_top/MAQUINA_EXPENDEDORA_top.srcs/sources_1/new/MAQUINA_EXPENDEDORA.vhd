----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.12.2021 09:45:02
-- Design Name: 
-- Module Name: MAQUINA_EXPENDEDORA_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAQUINA_EXPENDEDORA_top is
  Port (
        -- Entradas
        prod: IN std_logic_vector (3 DOWNTO 0);
        CLK: IN std_logic ;
        DIEZ_cent: IN std_logic;
        VEINTE_cent: IN std_logic;
        CINCUENTA_cent: IN std_logic;
        UNO_euro: IN std_logic;
        SENSOR_producto: IN std_logic;
        SENSOR_monedas: IN std_logic;
        RESET_FSM: IN std_logic;
        
        --Salidas
        display_segmentos: out std_logic_vector(7 downto 0);
        display_eleccion: out std_logic_vector(7 downto 0);
        expulsar: out std_logic_Vector(3 downto 0);
        monedas_fuera: out std_logic
  );
end MAQUINA_EXPENDEDORA_top;





architecture Behavioral of MAQUINA_EXPENDEDORA_top is
--------------------------------------------------------------------------------------
-------------------------------Senyales intermedias ----------------------------------------------------------------------
   
    --Sincronizador->EDGE_DETECTOR
    signal sync1: std_logic;
    signal sync2: std_logic;
    signal sync3: std_logic;
    signal sync4: std_logic;
    
    --Sincronizador_monedas->EDGE_DETECTOR_monedas
    signal sync10cent: std_logic;
    signal sync20cent: std_logic;
    signal sync50cent: std_logic;
    signal sync1euro: std_logic;
    
    --EDGE_DETECTOR_monedas->ACUMULAR 
    signal edege10cent: std_logic;
    signal edege20cent: std_logic;
    signal edege50cent: std_logic;
    signal edege1euro: std_logic;
    
    --EDGE_DETECTOR->COMPROBAR CANTIDAD
    signal edege1: std_logic;
    signal edege2: std_logic;
    signal edege3: std_logic;
    signal edege4: std_logic;
    
    --Acumulador->COMPPROBAR CANTIDAD
    signal valor_total: std_logic_vector (7 downto 0);
    
    --COMPROBAR CANTIDAD->Expulsar signal
    signal p: std_logic_vector (3 downto 0);
    
    --ACUMULADOR<->FSM
    signal hay_moneda: std_logic;
    signal acumular: std_logic;
    
    --COMPROBAR CANTIDAD <-> FSM 
    signal producto_elegido: std_logic;
    signal cantidad_mayor: std_logic;
    signal cantidad_correcta: std_logic;
    signal cantidad_menor: std_logic;
    signal comprobar: std_logic;
    
    --EXPULSAR PRODUCTO <-> FSM 
    signal expulsar_producto: std_logic;
    signal producto_expulsado: std_logic;
    
    --EXPULSAR MONEDAS <-> FSM 
    signal expulsar_monedas_fsm: std_logic;
    signal no_hay_monedas: std_logic;
    
    --RESET
    signal reset_entidades: std_logic_vector (4 downto 0);
    
    signal ningunaparte:std_logic;
    
----------------------------------------------------------------------------------------------------------------------    
---------------------------------Entidades-----------------------------------------------------------------------------------------------------------
COMPONENT EDGEDTCTR
    generic (
        CYCLES: positive := 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    );
    port (
        CLK : in std_logic;      -- Se�al de reloj
        SYNC_IN : in std_logic;  -- Entrada sincronizada
        EDGE : out std_logic	 -- Flanco detectado
    );
END component;

component  SYNCHRNZR is
    port (
        CLK : in std_logic;
        ASYNC_IN : in std_logic;
        SYNC_OUT : out std_logic
    );
end COMPONENT;

component  fsm is
port (
    RESET : in std_logic;
    CLK : in std_logic;
    MONEDA_IN: in std_logic;
    PROD_ELEGIDO: in std_logic;
    DINERO_MAYOR: in std_logic;
    DINERO_MENOR: in std_logic;
    DINERO_IGUAL: in std_logic;
    PROD_EXPULSADO: in std_logic;
    MONEDAS_EXP: in std_logic;
    ENABLE: out std_logic_vector(4 downto 0);
    RESET_ENTITIES: out std_logic_vector(4 downto 0) -- Se�ales de reset para las entidades
);
end component ;  

component acumulador is -- entidad que cuenta las distintas monedas que se van introduciendo
	generic (width: positive := 9); 
	port(
    reset: in std_logic;
    clk: in std_logic;
	diez : in std_logic; -- se?al relacionada a las monedas de diez centimos
	veinte: in std_logic; -- se?al relacionada a las monedas de veinte centimos
	cincuenta: in std_logic; -- se?al relacionada a las monedas de cincuenta centimos
	uno: in std_logic; -- se?al relacionada a las monedas de un euro
	valorfinal: out std_logic_vector (0 to 7); -- valor de la suma total
	hay_monedas: out std_logic ;
	acumular: in std_logic 
);
end component;

component visualizer is
    generic(
            DIGITS: positive := 3;
            RFRSH_RATE: positive := 30;
            CLK_FREQ: positive := 1e8
            );
    port (
          RESET_N    : in  std_logic;
          CLK        : in  std_logic;
          valor      : in std_logic_vector(7 downto 0); -- Valor recibido del acumulador
          SEGMNTS_N  : out std_logic_vector(7 downto 0);
          DIGITS_N   : out std_logic_vector(7 downto 0)
);
end component;

component cantidadyproducto is
generic (width: positive:=4);
	port(
    reset: in std_logic;
    clk: in std_logic;
	valortotal: in std_logic_vector (7 downto 0);
	boton: in std_logic_vector (width-1 downto 0);
	productoseleccionado: out std_logic;
	comprobar: in std_logic;
	producto: out std_logic_vector (width-1 downto 0);
	senal: out std_logic_vector (2 downto 0) -- segun el bit a 1 indicar� distintas cosas (0)=igual_monedas; (1)=menos_monedas; (2)=mas_monedas
);
end component;

component EXPULSAR_MONEDAS_ENTITY is
    Port ( sensor_monedas : in STD_LOGIC;
           expulsar_monedas : in STD_LOGIC;
           no_hay_monedas: out STD_LOGIC;
           monedas_fuera : out STD_LOGIC);
end component;

component  EXPULSAR_PRODUCTO_ENTITY is
    Generic (cantidad: integer:= 4);
    Port ( producto : in STD_LOGIC_VECTOR (cantidad-1 downto 0);
           expulsar_producto : in STD_LOGIC;
           producto_expulsado : out STD_LOGIC;
           sensor_producto: in STD_LOGIC;
           expulsar : out STD_LOGIC_VECTOR (cantidad-1 downto 0));
end component;

---------------------------------------------------------------------
------------------------- Asignacion de senyales -------------------------------------------
begin

--Sincronizadores botones
Boton_sync_1: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(3),
        SYNC_OUT => sync1
    );
Boton_sync_2: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(2),
        SYNC_OUT => sync2
    );
    
Boton_sync_3: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(1),
        SYNC_OUT => sync3
    );

Boton_sync_4: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => prod(0),
        SYNC_OUT => sync4
    );
    
 Boton_sync_10cent: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => DIEZ_cent,
        SYNC_OUT => sync10cent
    );
    
Boton_sync_20cent: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => VEINTE_cent,
        SYNC_OUT => sync20cent
    );

Boton_sync_50cent: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => CINCUENTA_cent,
        SYNC_OUT => sync50cent
    );
    
Boton_sync_1euro: SYNCHRNZR
    port map (
        CLK => CLK,
        ASYNC_IN => UNO_euro,
        SYNC_OUT => sync1euro
    );
    
--Detector de flancos
 Boton_edge_1: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync1,  -- Entrada sincronizada
        EDGE => edege1	 -- Flanco detectado
    );

 Boton_edge_2: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync2,  -- Entrada sincronizada
        EDGE => edege2	 -- Flanco detectado
    );
 
Boton_edge_3: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync3,  -- Entrada sincronizada
        EDGE => edege3	 -- Flanco detectado
    );

 Boton_edge_4: EDGEDTCTR
    generic map(
        CYCLES => 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync4,  -- Entrada sincronizada
        EDGE => edege4	 -- Flanco detectado
    );

Boton_edge_10cent: EDGEDTCTR
    generic map(
        CYCLES => 8    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync10cent,  -- Entrada sincronizada
        EDGE => edege10cent	 -- Flanco detectado
    );

Boton_edge_20cent: EDGEDTCTR
    generic map(
        CYCLES => 8    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync20cent,  -- Entrada sincronizada
        EDGE => edege20cent	 -- Flanco detectado
    );

Boton_edge_50: EDGEDTCTR
    generic map(
        CYCLES => 8    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync50cent,  -- Entrada sincronizada
        EDGE => edege50cent	 -- Flanco detectado
    );

Boton_edge_1euro: EDGEDTCTR
    generic map(
        CYCLES => 8    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    )
    port map (
        CLK => clk,      -- Se�al de reloj
        SYNC_IN => sync1euro,  -- Entrada sincronizada
        EDGE => edege1euro	 -- Flanco detectado
    );
-- Maquina de estados (FSM)
MaquinaEstados: fsm 
port map(
    RESET => NOT RESET_FSM,
    CLK => clk,
    MONEDA_IN => hay_moneda,
    PROD_ELEGIDO => producto_elegido,
    DINERO_MAYOR =>cantidad_mayor,
    DINERO_MENOR => cantidad_menor,
    DINERO_IGUAL => cantidad_correcta,
    PROD_EXPULSADO => producto_expulsado,
    MONEDAS_EXP => no_hay_monedas,
    ENABLE(0)=> ningunaparte,
    ENABLE(1)=> acumular,
    ENABLE(2)=> comprobar,
    ENABLE(3)=> expulsar_producto,
    ENABLE(4)=> expulsar_monedas_fsm,
    RESET_ENTITIES => reset_entidades -- Se�ales de reset para las entidadesRESET_FSM
);  

-- Acumulador de la cantidad introducida
AcumuladorEntity: acumulador  
	generic map (width => 9) 
	port map(
     reset => reset_entidades(1) OR NOT RESET_FSM,
     clk => CLK,
     diez => edege10cent, -- se?al relacionada a las monedas de diez centimos
     veinte => edege20cent, -- se?al relacionada a las monedas de veinte centimos
     cincuenta => edege50cent, -- se?al relacionada a las monedas de cincuenta centimos
     uno => edege1euro, -- se?al relacionada a las monedas de un euro
     valorfinal => valor_total, -- valor de la suma total
     hay_monedas =>hay_moneda,
     acumular => acumular
);

visualizador: visualizer
    generic map(
            DIGITS => 3,
            RFRSH_RATE => 60,
            CLK_FREQ => 1e8
            )
    port map(
          RESET_N => not reset_entidades(1),
          CLK=> CLK,
          valor =>valor_total, -- Valor recibido del acumulador
          SEGMNTS_N => display_segmentos,
          DIGITS_N  => display_eleccion
);

-- Comprobar cantidad y seleccion de producto
CantidadProducto: cantidadyproducto 
generic map(width => 4)
	port map (
    reset =>  reset_entidades(2),
    clk => CLK,
	valortotal => valor_total,
	productoseleccionado=>producto_elegido,
	boton(3) => edege1,
	boton(2) => edege2,
	boton(1) => edege3,
	boton(0) => edege4,
	comprobar => comprobar,
	producto => p,
	senal(2) => cantidad_mayor,
    senal(1) => cantidad_menor,
    senal(0) => cantidad_correcta
);


-- Expulsar monedas
ExpulsarMonedas: EXPULSAR_MONEDAS_ENTITY
    Port map( sensor_monedas=> SENSOR_monedas,
           expulsar_monedas => expulsar_monedas_fsm,
           no_hay_monedas => no_hay_monedas,
           monedas_fuera => monedas_fuera
);

-- Expulsar producto elegido
ExpulsarProducto:  EXPULSAR_PRODUCTO_ENTITY 
    Generic map (cantidad => 4)
    Port map( producto => p,
           expulsar_producto => expulsar_producto,
           producto_expulsado => producto_expulsado,
           sensor_producto => SENSOR_producto,
           expulsar => expulsar
    );

end Behavioral;
