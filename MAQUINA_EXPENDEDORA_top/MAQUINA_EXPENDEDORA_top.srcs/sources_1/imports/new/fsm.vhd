------------------------------------------------------------------------------------
---- Company: 
---- Engineer: 
---- 
---- Create Date: 05.11.2021 22:06:51
---- Design Name: 
---- Module Name: fsm - Behavioral
---- Project Name: 
---- Target Devices: 
---- Tool Versions: 
---- Description: 
---- 
---- Dependencies: 
---- 
---- Revision:
---- Revision 0.01 - File Created
---- Additional Comments:
---- 
------------------------------------------------------------------------------------


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
----use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx leaf cells in this code.
----library UNISIM;
----use UNISIM.VComponents.all;

--entity fsm is
--port (
--    RESET : in std_logic;
--    CLK : in std_logic;
--    MONEDA_IN: in std_logic;
--    PROD_ELEGIDO: in std_logic;
--    DINERO_MAYOR: in std_logic;
--    DINERO_MENOR: in std_logic;
--    DINERO_IGUAL: in std_logic;
--    PROD_EXPULSADO: in std_logic;
--    MONEDAS_EXP: in std_logic;
--    ENABLE: out std_logic_vector(4 downto 0);
--    RESET_ENTITIES: out std_logic_vector(4 downto 0) -- Se�ales de reset para las entidades
--);
--end fsm;

--architecture behavioral of fsm is
--    type STATES is (S0, S1, S2, S3, S4);
--    -- S0: reposo
--    -- S1: acumular 
--    -- S2: comprobar 
--    -- S3: expulsar producto 
--    -- S4: expulsar monedas
--    signal current_state: STATES := S0;
--    signal next_state: STATES;
--begin
--    state_register: process (RESET, CLK)
--    begin
--    if RESET = '1' then
--        current_state <= S0;
--    elsif rising_edge(CLK) then
--        current_state <= next_state; -- Actualiza el estado actual
--    end if;
--    end process;
    
--nextstate_decod: process (MONEDA_IN, PROD_ELEGIDO, DINERO_IGUAL, DINERO_MAYOR, DINERO_MENOR, PROD_EXPULSADO, MONEDAS_EXP, current_state)
--begin
--    next_state <= current_state;
--    case current_state is
--        when S0 => -- Estado de reposo
--            if MONEDA_IN = '1' then
--                next_state <= S1;
--            end if;
--        when S1 => -- Estado de acumular valor total
--            if PROD_ELEGIDO = '1' then
--                next_state <= S2;
--            end if;
--        when S2 => -- Estado de comprobaci�n del valor introducido
--            if DINERO_MENOR = '1' then
--                next_state <= S1;
--                ENABLE(2) <= '0';
--                RESET_ENTITIES(2) <= '1';
--            elsif DINERO_MAYOR = '1' then
--                next_state <= S4;
--            elsif DINERO_IGUAL = '1' then
--                next_state <= S3;
--            end if;
--        when S3 => -- Estado de expulsi�n de producto
--            if PROD_EXPULSADO = '1' then
--                next_state <= S0;
--            end if;
--        when S4 => -- Estado de expulsi�n de monedas
--            if MONEDAS_EXP = '1' then
--                next_state <= S0;
--            end if;
--        when others =>
--                next_state <= S0;
--    end case;
--end process;

--output_decod: process (current_state)
--begin
--    --ENABLE <= (OTHERS => '0');
--   -- RESET_ENTITIES <= (OTHERS => '0');
--    case current_state is
--        when S0 =>
--            ENABLE(0) <= '1';
--            RESET_ENTITIES(0) <= '0';
--            RESET_ENTITIES(4 downto 1) <= "1111", "0000" after 5ns;
--            ENABLE(4 downto 1)<="0000";
--        when S1 =>
--            ENABLE(1) <= '1';
--            RESET_ENTITIES(1) <= '0';
--            RESET_ENTITIES(0) <= '1', '0' after 5ns;
--            ENABLE(0) <= '0';
--        when S2 =>
--            RESET_ENTITIES(2) <= '0';
--            ENABLE(2) <= '1';
--            ENABLE(1) <= '0';
--            RESET_ENTITIES(1) <= '1', '0' after 5ns;
--        when S3 =>
--            ENABLE(3) <= '1';
--            ENABLE(2) <= '0';
--            RESET_ENTITIES(2) <= '1', '0' after 5ns;
--            RESET_ENTITIES(3) <= '0';
--        when S4 =>
--            ENABLE(4) <= '1';
--            ENABLE(2) <= '0';
--            RESET_ENTITIES(2) <= '1', '0' after 5ns;
--            RESET_ENTITIES(4) <= '0';
--        when others =>
--            ENABLE <= (OTHERS => '0');
--            RESET_ENTITIES <= (OTHERS => '1');
--    end case;
--end process;

--end behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.11.2021 22:06:51
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm is
port (
    RESET : in std_logic;
    CLK : in std_logic;
    MONEDA_IN: in std_logic;
    PROD_ELEGIDO: in std_logic;
    DINERO_MAYOR: in std_logic;
    DINERO_MENOR: in std_logic;
    DINERO_IGUAL: in std_logic;
    PROD_EXPULSADO: in std_logic;
    MONEDAS_EXP: in std_logic;
    ENABLE: out std_logic_vector(4 downto 0);
    RESET_ENTITIES: out std_logic_vector(4 downto 0) -- Se?ales de reset para las entidades
);
end fsm;

architecture behavioral of fsm is
    type STATES is (S0, S1, S2, S3, S4);
    -- S0: reposo
    -- S1: acumular 
    -- S2: comprobar 
    -- S3: expulsar producto 
    -- S4: expulsar monedas
    signal current_state: STATES := S0;
    signal next_state: STATES;
begin
    state_register: process (RESET, CLK)
    begin
    if RESET = '1' then
        current_state <= S0;
    elsif rising_edge(CLK) then
        current_state <= next_state; -- Actualiza el estado actual
    end if;
    end process;
    
nextstate_decod: process (MONEDA_IN, PROD_ELEGIDO, DINERO_IGUAL, DINERO_MAYOR, DINERO_MENOR, PROD_EXPULSADO, MONEDAS_EXP, current_state)
begin
    next_state <= current_state;
    case current_state is
        when S0 => -- Estado de reposo
            if MONEDA_IN = '1' then
                next_state <= S1;
            end if;
        when S1 => -- Estado de acumular valor total
            if PROD_ELEGIDO = '1' then
                next_state <= S2;
            end if;
        when S2 => -- Estado de comprobaci?n del valor introducido
            if DINERO_MENOR = '1' then
                next_state <= S1;
            elsif DINERO_MAYOR = '1' then
                next_state <= S4;
            elsif DINERO_IGUAL = '1' then
                next_state <= S3;
            end if;
        when S3 => -- Estado de expulsi?n de producto
            if PROD_EXPULSADO = '1' then
                next_state <= S0;
            end if;
        when S4 => -- Estado de expulsi?n de monedas
            if MONEDAS_EXP = '1' then
                next_state <= S0;
            end if;
        when others =>
                next_state <= S0;
    end case;
end process;

output_decod: process (current_state)
begin
    ENABLE <= (OTHERS => '0');
    --RESET_ENTITIES <= (OTHERS => '1');
    RESET_ENTITIES <= (OTHERS => '0');
    case current_state is
        when S0 =>
            ENABLE(0) <= '1';
            RESET_ENTITIES(2) <= '1';
            --RESET_ENTITIES(0) <= '0';
        when S1 =>
            ENABLE(1) <= '1';
            --RESET_ENTITIES(1) <= '0';
        when S2 =>
            ENABLE(2) <= '1';
            --RESET_ENTITIES(2) <= '0';
        when S3 =>
            ENABLE(3) <= '1';
            --RESET_ENTITIES(3) <= '0';
            RESET_ENTITIES(0) <= '1';
            RESET_ENTITIES(1) <= '1';
            --RESET_ENTITIES(2) <= '1';
        when S4 =>
            ENABLE(4) <= '1';
            --RESET_ENTITIES(4) <= '0';
            RESET_ENTITIES(0) <= '1';
            RESET_ENTITIES(1) <= '1';
            RESET_ENTITIES(2) <= '1';
        when others =>
            ENABLE <= (OTHERS => '0');
            RESET_ENTITIES <= (OTHERS => '1');
    end case;
end process;

end behavioral;
