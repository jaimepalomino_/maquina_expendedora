library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EDGEDTCTR is
    generic (
        CYCLES: positive := 4    -- N�mero de ciclos que deben transcurrir para considerar un flanco de bajada v�lido
    );
    port (
        CLK : in std_logic;      -- Se�al de reloj
        SYNC_IN : in std_logic;  -- Entrada sincronizada
        EDGE : out std_logic	 -- Flanco detectado
    );
end EDGEDTCTR;

architecture BEHAVIORAL of EDGEDTCTR is
    
begin
    process (CLK)
    variable sreg : std_logic_vector(CYCLES downto 0);                          -- Almacena los valores de SYNC_IN
    variable zeros : std_logic_vector(sreg'high-1 downto 0) := (others => '0'); -- Generar un vector de ceros cuyo tama�o depende del gen�rico CYCLES
    begin
       if rising_edge(CLK) then
           sreg := sreg(sreg'high-1 downto 0) & SYNC_IN;
           if sreg = '1' & zeros then
            EDGE <= '1';
           else
            EDGE <= '0';
           end if;
       end if;
    end process;

end BEHAVIORAL;