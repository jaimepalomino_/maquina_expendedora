----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.12.2021 18:21:53
-- Design Name: 
-- Module Name: visualizer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity visualizer is
    generic(
            DIGITS: positive := 3;
            RFRSH_RATE: positive := 30;
            CLK_FREQ: positive := 1e8
            );
    port (
          RESET_N    : in  std_logic;
          CLK        : in  std_logic;
          valor      : in std_logic_vector(7 downto 0); -- Valor recibido del acumulador
          SEGMNTS_N  : out std_logic_vector(7 downto 0);
          DIGITS_N   : out std_logic_vector(7 downto 0)
);
end visualizer;

architecture structural of visualizer is

    -- Componentes
    
    component preparar_num is
      port ( 
            valor: in std_logic_vector(7 downto 0);
            digito0: out std_logic_vector(3 downto 0);
            digito1: out std_logic_vector(3 downto 0);
            digito2: out std_logic_vector(3 downto 0)
            );
    end component;    
    
    component muxer is
      generic (
        DIGITS : positive := 3
      );
      port (
        SEL     : in  std_logic_vector(DIGITS - 1 downto 0);
        display0: in std_logic_vector(3 downto 0); -- se?al para el display de unidades de los centimos (siempre con un 0 por la naturaleza de las monedas insertadas)
        display1: in std_logic_vector(3 downto 0); -- se?al para el display de decenas de centimos
        display2: in std_logic_vector(3 downto 0); -- se?al para el display de centenas de centimos (los euros)
        BCD_OUT : out std_logic_vector(3 downto 0)
      );
    end component;
    
    component TIMER is
      generic (
        MODULO  : positive := 416667
      );
      port ( 
        RESET_N : in   std_logic;
        CLK     : in   std_logic;
        STROBE  : out  std_logic
      );
    end component;

    component SCANNER is
      generic (
        DIGITS : positive := 3
      );
      port (
        RESET_N : in  std_logic;
        CLK     : in  std_logic;
        CE      : in  std_logic;
        SEL     : out std_logic_vector(DIGITS - 1 downto 0)
      );
    end component;

    component decoder IS
        PORT (
            code : IN std_logic_vector(3 DOWNTO 0);
            led : OUT std_logic_vector(6 DOWNTO 0)
        );
    END component;
    
    -- Se�ales
    
    signal clk_strobe : std_logic;
    signal deco_in    : std_logic_vector(3 downto 0);
  --  signal segments   : std_logic_vector(6 downto 0);
    signal sel        : std_logic_vector(DIGITS - 1 downto 0);  
    signal dig0       : std_logic_vector(3 downto 0);
    signal dig1       : std_logic_vector(3 downto 0);
    signal dig2       : std_logic_vector(3 downto 0);
    
begin

  timer1: TIMER
    generic map (
      MODULO  => CLK_FREQ / (RFRSH_RATE * DIGITS)
--        MODULO => 10  -- SOLO SIMULACION 
    )
    port map ( 
      RESET_N => RESET_N,
      CLK     => CLK,
      STROBE  => clk_strobe
    );

  scanner1: SCANNER
    generic map (
      DIGITS => DIGITS
    )
    port map (
      RESET_N => RESET_N,
      CLK     => CLK,
      CE      => clk_strobe,
      SEL     => sel
    );
    
    
  prep1: preparar_num
    port map (
      valor   => valor,
      digito0 => dig0, 
      digito1 => dig1,
      digito2 => dig2
      );

  muxer1: MUXER
    generic map (
      DIGITS => DIGITS
    )
    port map (
      SEL       => sel,
      display0  => dig0,
      display1  => dig1,
      display2  => dig2,
      BCD_OUT   => deco_in
    );

  decod1: decoder
    port map (
      code   => deco_in,
      led => SEGMNTS_N(SEGMNTS_N'high downto 1)
    );

  -- Punto decimal
  with sel select
      SEGMNTS_N(0) <= '0' when "011",
                      '1' when others;

  -- Apagar displays que no se utilicen y encender el indicado
  DIGITS_N(DIGITS_N'HIGH downto DIGITS) <= (others => '1');
  DIGITS_N(DIGITS - 1 downto 0) <= sel;

end structural;
