----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.12.2021 12:28:00
-- Design Name: 
-- Module Name: preparar_num - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity preparar_num is
  port ( 
        valor: in std_logic_vector(7 downto 0);
        digito0: out std_logic_vector(3 downto 0);
        digito1: out std_logic_vector(3 downto 0);
        digito2: out std_logic_vector(3 downto 0)
        );
end preparar_num;

architecture Behavioral of preparar_num is
    

begin

process(valor)
    variable valoraux: unsigned(7 downto 0);
    variable digito0aux: unsigned(3 downto 0);
    variable digito1aux: unsigned(3 downto 0);
    variable digito2aux: unsigned(3 downto 0);
begin
    valoraux := unsigned(valor);
    
    -- Unidades de euro
    digito2aux := resize(valoraux/100,4);
    
    -- Decenas de c�ntimos
    digito1aux := resize((valoraux mod 100)/10,4);
    
    -- Unidades de c�ntimos
    digito0aux := resize((valoraux mod 100) mod 10,4);
    
    -- Asignaci�n de se�ales 
    digito2 <= std_logic_vector(digito2aux);
    digito1 <= std_logic_vector(digito1aux);
    digito0 <= std_logic_vector(digito0aux);


end process;

end Behavioral;
