library IEEE;
use IEEE.STD_LOGIC_1164.all;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity acumulador is -- entidad que cuenta las distintas monedas que se van introduciendo
	generic (width: positive := 9); 
	port(
    
    reset: in std_logic;
    clk: in std_logic;

	diez : in std_logic; -- se?al relacionada a las monedas de diez centimos
	veinte: in std_logic; -- se?al relacionada a las monedas de veinte centimos
	cincuenta: in std_logic; -- se?al relacionada a las monedas de cincuenta centimos
	uno: in std_logic; -- se?al relacionada a las monedas de un euro
	valorfinal: out std_logic_vector (7 downto 0); -- valor de la suma total
	
	hay_monedas: out std_logic ;
	acumular: in std_logic 
);
end acumulador;

architecture behavioral of acumulador is

	signal valoraux: std_logic_vector (0 to 7) := "00000000"; --se?al auxiliar para leer el valor de la suma total

begin
	process(diez, veinte, cincuenta, uno, reset, acumular) 
	begin
	if rising_edge(clk) then
	if (diez = '1' or veinte = '1' or cincuenta = '1' or uno = '1') then
		hay_monedas<='1';

        if diez = '1' then
            valoraux <= valoraux + "1010";
        elsif veinte = '1' then
            valoraux <= valoraux + "10100";
        elsif cincuenta = '1' then
            valoraux <= valoraux + "110010";
        elsif uno = '1' then
            valoraux <= valoraux + "1100100";
        else
            valoraux <= valoraux;
        end if;
    end if;


	end if;
    if reset = '1' then
    	valoraux <= "00000000";
        hay_monedas <= '0';
    end if;
	end process;

	--asignaciones de las se?ales correspondientes 

	valorfinal<=valoraux;

end architecture;  
