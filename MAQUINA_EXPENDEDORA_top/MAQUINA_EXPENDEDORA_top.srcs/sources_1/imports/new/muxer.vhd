----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.12.2021 16:42:12
-- Design Name: 
-- Module Name: muxer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity muxer is
  generic (
    DIGITS : positive := 3
  );
  port (
    SEL     : in  std_logic_vector(DIGITS - 1 downto 0);
    display0: in std_logic_vector(3 downto 0); -- se?al para el display de unidades de los centimos (siempre con un 0 por la naturaleza de las monedas insertadas)
	display1: in std_logic_vector(3 downto 0); -- se?al para el display de decenas de centimos
	display2: in std_logic_vector(3 downto 0); -- se?al para el display de centenas de centimos (los euros)
	BCD_OUT : out std_logic_vector(3 downto 0)
  );
end muxer;

architecture Behavioral of muxer is

begin
--process(SEL)
--begin
--    case SEL is
--        when "110" =>
--            BCD_OUT <= display0;
--        when "101" =>
--            BCD_OUT <= display1;
--        when "011" =>
--            BCD_OUT <= display2;
--        when others =>
--            BCD_OUT <= "1111110";
--    end case;
--end process;
    with SEL select
        BCD_OUT <= display0 when "110",
                   display1 when "101",
                   display2 when "011",
                   "1111"  when others; -- guion de error

end Behavioral;
