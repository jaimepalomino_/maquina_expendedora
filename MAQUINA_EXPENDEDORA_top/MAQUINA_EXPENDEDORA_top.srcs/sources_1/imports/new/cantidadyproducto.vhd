library IEEE;
use IEEE.STD_LOGIC_1164.all;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity cantidadyproducto is
generic (width: positive:=4);
	port(
    
    reset: in std_logic;
    clk: in std_logic;

	valortotal: in std_logic_vector (7 downto 0);
	boton: in std_logic_vector (width-1 downto 0);

	productoseleccionado: out std_logic;
	comprobar: in std_logic;

	producto: out std_logic_vector (width-1 downto 0);
	senal: out std_logic_vector (2 downto 0) -- segun el bit a 1 indicar� distintas cosas (0)=igual_monedas; (1)=menos_monedas; (2)=mas_monedas
);
end cantidadyproducto;

architecture structural of cantidadyproducto is
signal boton_seleccionado: std_logic_vector (width-1 downto 0);
begin 
	process(boton,reset,comprobar,clk)
    begin
    if rising_edge(clk) then
    productoseleccionado <= '0';
    if reset = '1' then
    	productoseleccionado <= '0';
    	producto <=(others=>'0');
    	senal<=(others=>'0');
--	elsif ((rising_edge(boton(0)) or rising_edge(boton(1)) or rising_edge(boton(2)) or rising_edge(boton(3))) and comprobar='0' ) then
    elsif ((boton(0)='1' or boton(1)='1' or boton(2)='1' or boton(3)='1') and comprobar='0' ) then
		productoseleccionado<='1';
		boton_seleccionado <= boton;
	elsif (comprobar='1') then
	senal<=(others=>'0'); -- se inicializa a 0 para asegurar que no hay datos anteriores guardados
		if (valortotal="01100100") then
			producto<=boton_seleccionado;
			senal<=(senal'low=>'1' , others => '0'); --senal="100" tener dinero justo
			
		else
        
        	if valortotal<"1100100" then
				senal<=(senal'low+1=>'1' , others => '0'); -- senal= "010" no tener suficiente dinero
			else 
				senal<=(senal'high=>'1', others=>'0'); -- senal= "001" tener monedas de m�s
			end if;
		end if;
    end if;
	end if;
    end process;
    
--    productoseleccionado <= '0' when reset = '1';
--    process(reset)
--    begin
--    	if reset = '1' then
--        	productoseleccionado <= '0';
--        end if;
--    end process;    
	
    
 
    
end architecture;
