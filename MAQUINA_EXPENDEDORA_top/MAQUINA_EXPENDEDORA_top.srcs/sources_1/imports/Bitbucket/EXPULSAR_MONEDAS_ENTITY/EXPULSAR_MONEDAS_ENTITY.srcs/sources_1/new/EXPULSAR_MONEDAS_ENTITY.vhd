----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.12.2021 15:34:08
-- Design Name: 
-- Module Name: EXPULSAR_MONEDAS_ENTITY - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXPULSAR_MONEDAS_ENTITY is
    Port ( sensor_monedas : in STD_LOGIC;
           expulsar_monedas : in STD_LOGIC;
           no_hay_monedas: out STD_LOGIC;
           monedas_fuera : out STD_LOGIC);
end EXPULSAR_MONEDAS_ENTITY;

architecture Behavioral of EXPULSAR_MONEDAS_ENTITY is
begin
    process(expulsar_monedas)
    begin
        if (expulsar_monedas='1') then
            monedas_fuera <= '1';
        else 
            monedas_fuera <= '0';
        end if;
    end process;
    
     no_hay_monedas  <= sensor_monedas;
      
end Behavioral;

