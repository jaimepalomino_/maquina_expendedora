----------------------------------------------------------------------------------
-- Company: Universudad Politécnica de Madrid
-- Engineer student: Jaime Palomino Vaquero 
-- 
-- Design Name: EXULSAR_PRODUCTO
-- Module Name: EXULSAR_PRODUCTO - Behavioral
-- Project Name: Maquina_refrescos 
-- Target Devices: Nexys 4 DDR
-- Tool Versions: V0.0
-- Description: Entidad que se encaarga de expulsar el producto deseado accionando 
--              el actuador adecuado.
-- 
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EXPULSAR_PRODUCTO_ENTITY is
    Generic (cantidad: integer:= 4);
    Port ( producto : in STD_LOGIC_VECTOR (cantidad-1 downto 0);
           expulsar_producto : in STD_LOGIC;
           producto_expulsado : out STD_LOGIC;
           sensor_producto: in STD_LOGIC;
           expulsar : out STD_LOGIC_VECTOR (cantidad-1 downto 0));
end EXPULSAR_PRODUCTO_ENTITY;

architecture Behavioral of EXPULSAR_PRODUCTO_ENTITY is
begin
    process (expulsar_producto)
    begin
        IF (expulsar_producto='1') THEN
            expulsar <= producto;
        ELSE
        	expulsar <= (others => '0');     
        END IF;
      
    end PROCESS;
    
    producto_expulsado <= sensor_producto;
end Behavioral;

