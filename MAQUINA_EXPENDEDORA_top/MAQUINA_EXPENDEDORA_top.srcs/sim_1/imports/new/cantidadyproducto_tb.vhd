-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 21.12.2021 11:39:00 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_cantidadyproducto is
end tb_cantidadyproducto;

architecture tb of tb_cantidadyproducto is

    component cantidadyproducto
    generic (width: positive:=4);
        port (
        	  reset                : in std_logic;
        	  valortotal           : in std_logic_vector (0 to 7);
              boton                : in std_logic_vector (width-1 downto 0);
              productoseleccionado : out std_logic;
              comprobar            : in std_logic;
              producto             : out std_logic_vector (width-1 downto 0);
              senal                : out std_logic_vector (0 to 2));
    end component;
	signal reset                : std_logic;
    signal valortotal           : std_logic_vector (0 to 7);
    signal boton                : std_logic_vector (3 downto 0);
    signal productoseleccionado : std_logic;
    signal comprobar            : std_logic;
    signal producto             : std_logic_vector (3 downto 0);
    signal senal                : std_logic_vector (0 to 2);

begin

    dut : cantidadyproducto
    port map (
    		  reset                => reset,
              valortotal           => valortotal,
              boton                => boton,
              productoseleccionado => productoseleccionado,
              comprobar            => comprobar,
              producto             => producto,
              senal                => senal);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        valortotal <= (others => '0');
        boton <= (others => '0');
        comprobar <= '0';
        reset <= '1';
        wait for 10 ns;

        -- EDIT Add stimuli here
        valortotal <= "00001000";
        boton <= "0100";
        comprobar <= '0';
        reset <= '1';
        wait for 10 ns;
        
        comprobar <= '1';
        reset <= '0';
        wait for 10 ns;
        
        
        valortotal <= "01100100";
        boton <= "0010";
        comprobar <= '0';
        reset <= '1';
        wait for 10 ns;
        
        comprobar <= '1';
        reset <= '0';
        wait for 10 ns;
        
        valortotal <= "00001000";
        boton <= "0010";
        comprobar <= '0';
        reset <= '1';
        wait for 10 ns;
        
        comprobar <= '1';
        reset <= '0';
        wait for 10 ns;
        
        
        valortotal <= "01100100";
        boton <= "0100";
        comprobar <= '0';
        reset <= '1';
        wait for 10 ns;
        
        comprobar <= '1';
        reset <= '0';
        wait for 10 ns;
        
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

--configuration cfg_tb_cantidadyproducto of tb_cantidadyproducto is
--    for tb
--    end for;
--end cfg_tb_cantidadyproducto;