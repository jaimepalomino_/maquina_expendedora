-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 22.12.2021 10:11:06 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_acumulador is
end tb_acumulador;

architecture tb of tb_acumulador is

    component acumulador
    	generic (width: positive := 9);
        port (
        	  --reset       : in std_logic;
        	  valorinicial: in std_logic_vector (3 downto 0); 
        	  diez        : in std_logic;
              veinte      : in std_logic;
              cincuenta   : in std_logic;
              uno         : in std_logic;
              valorfinal  : out std_logic_vector (7 downto 0);
              
              --hay_monedas : out std_logic;
              acumular    : in std_logic 
              );
          
    end component;
    
    --signal reset	   : std_logic;
    signal valorinicial: std_logic_vector (3 downto 0); 
    signal diez        : std_logic;
    signal veinte      : std_logic;
    signal cincuenta   : std_logic;
    signal uno         : std_logic;
    signal valorfinal  : std_logic_vector (0 to 7);
    --signal hay_monedas : std_logic;
    signal acumular    : std_logic;
   

begin

    dut : acumulador
    port map (
    		 -- reset 	  => reset,
    		  valorinicial => valorinicial,
    		  diez        => diez,
              veinte      => veinte,
              cincuenta   => cincuenta,
              uno         => uno,
              valorfinal  => valorfinal,
              --hay_monedas => hay_monedas,
              acumular    => acumular
              );

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        valorinicial <= "0000";
        diez <= '0';
        veinte <= '0';
        cincuenta <= '0';
        uno <= '0';
        acumular <= '0';
        wait for 10 ns;
        
        valorinicial <= "0001";
        wait for 10 ns;
        
        acumular <= '1';
        wait for 10 ns;
        
        diez <= '1';
        wait for 10 ns;
        
        diez <= '0';
        wait for 10 ns;
        
        veinte <= '1';
        wait for 10 ns;
        
        veinte <= '0';
        wait for 10 ns;
        
        acumular <= '0';
        wait for 10 ns;

        -- EDIT Add stimuli here

        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

--configuration cfg_tb_acumulador of tb_acumulador is
--    for tb
--    end for;
--end cfg_tb_acumulador;