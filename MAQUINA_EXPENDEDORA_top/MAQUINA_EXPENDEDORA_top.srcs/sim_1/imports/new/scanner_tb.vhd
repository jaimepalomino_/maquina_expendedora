--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:54:07 11/22/2019
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/etsidi/docencia/Plan2010/grado/sed/2019-2020/pruebas/event_cntr/scanner_tb.vhd
-- Project Name:  event_cntr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SCANNER
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
 
entity SCANNER_TB is
end SCANNER_TB;

architecture TEST of SCANNER_TB is 

  -- Component Declaration for the Unit Under Test (UUT)
  component SCANNER
    generic (
      DIGITS : positive
    );
    port (
      RESET_N : in  std_logic;
      CLK     : in  std_logic;
      CE      : in  std_logic;
      SEL     : out std_logic_vector(DIGITS - 1 downto 0)
    );
  end component;   

  -- Constants
  constant clk_period   : time := 10 ns;
  constant delay        : time := 0.1 * clk_period;
  constant mux_channels : positive := 7;

  --Inputs
  signal reset_n : std_logic;
  signal clk     : std_logic;
  signal ce      : std_logic;

  --Outputs
  signal sel : std_logic_vector(mux_channels - 1 downto 0);
 
begin
 
  -- Instantiate the Unit Under Test (UUT)
  uut: SCANNER
    generic map (
      DIGITS  => mux_channels
    )
    port map (
      RESET_N => reset_n,
      CLK     => clk,
      CE      => ce,
      SEL     => sel
    );

  -- Clock process definitions
  clk_process :process
  begin
    CLK <= '0';
    wait for 0.5 * clk_period;
    CLK <= '1';
    wait for 0.5 * clk_period;
  end process;

  -- Reset pulse
  reset_n <= '0' after 0.25 * clk_period, '1' after 0.75 * clk_period;

  -- Stimulus process
  stim_proc: process
  begin
    for i in 1 to 2 * mux_channels + 2 loop
      if i = mux_channels + 1 then
        ce <= '0' after delay;
      else
        ce <= '1' after delay;
      end if;
      wait until clk = '1';
    end loop;

    wait for 0.25 * clk_period;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end;
