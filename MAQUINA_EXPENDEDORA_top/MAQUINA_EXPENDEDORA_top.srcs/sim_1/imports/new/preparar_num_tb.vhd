-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 26.12.2021 11:44:48 UTC

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_preparar_num is
end tb_preparar_num;

architecture tb of tb_preparar_num is

    component preparar_num
        port (valor   : in std_logic_vector (7 downto 0);
              digito0 : out std_logic_vector (3 downto 0);
              digito1 : out std_logic_vector (3 downto 0);
              digito2 : out std_logic_vector (3 downto 0));
    end component;

    signal valor   : std_logic_vector (7 downto 0);
    signal digito0 : std_logic_vector (3 downto 0);
    signal digito1 : std_logic_vector (3 downto 0);
    signal digito2 : std_logic_vector (3 downto 0);
    
    TYPE vtest is record
        code : std_logic_vector(3 DOWNTO 0);
        led : std_logic_vector(6 DOWNTO 0);
    END RECORD;
    
    TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;
    CONSTANT test: vtest_vector := (
        (code => "0000", led => "0000001"),
        (code => "0001", led => "1001111"),
        (code => "0010", led => "0010010"),
        (code => "0011", led => "0000110"),
        (code => "0100", led => "1001100"),
        (code => "0101", led => "0100100"),
        (code => "0110", led => "0100000"),
        (code => "0111", led => "0001111"),
        (code => "1000", led => "0000000"),
        (code => "1001", led => "0000100"),
        (code => "1010", led => "1111110"),
        (code => "1011", led => "1111110"),
        (code => "1100", led => "1111110"),
        (code => "1101", led => "1111110"),
        (code => "1110", led => "1111110"),
        (code => "1111", led => "1111110")
        );
begin

    dut : preparar_num
    port map (valor   => valor,
              digito0 => digito0,
              digito1 => digito1,
              digito2 => digito2);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        valor <= (others => '0');

        -- EDIT Add stimuli here
        
        -- Digito 0
        FOR i IN 0 TO 200 LOOP
                valor <= std_logic_vector(to_unsigned(i,8));
            WAIT FOR 20 ns;
            ASSERT digito0 = test((i mod 100) mod 10).code
                REPORT "Salida incorrecta Digito 0."
                SEVERITY FAILURE;
        END LOOP;
        
        -- Digito 1
        FOR i IN 0 TO 200 LOOP
                valor <= std_logic_vector(to_unsigned(i,8));
            WAIT FOR 20 ns;
            ASSERT digito1 = test((i mod 100)/10).code
                REPORT "Salida incorrecta Digito 1."
                SEVERITY FAILURE;
        END LOOP;
        
        -- Digito 2
        FOR i IN 0 TO 200 LOOP
                valor <= std_logic_vector(to_unsigned(i,8));
            WAIT FOR 20 ns;
            ASSERT digito2 = test(i / 100).code
                REPORT "Salida incorrecta Digito 2."
                SEVERITY FAILURE;
        END LOOP;
        
        ASSERT false
            REPORT "Simulacin finalizada. Test superado."
            SEVERITY FAILURE;

        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

--configuration cfg_tb_preparar_num of tb_preparar_num is
--    for tb
--    end for;
--end cfg_tb_preparar_num;