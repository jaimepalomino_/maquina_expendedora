--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:03:16 11/22/2019
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/etsidi/docencia/Plan2010/grado/sed/2019-2020/pruebas/event_cntr/muxer_tb.vhd
-- Project Name:  event_cntr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: MUXER
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MUXER_TB is
end MUXER_TB;
 
architecture behavior of MUXER_TB is 
 
  -- Component Declaration for the Unit Under Test (UUT)

    component muxer is
      generic (
        DIGITS : positive := 3
      );
      port (
        SEL     : in  std_logic_vector(DIGITS - 1 downto 0);
        display0: in std_logic_vector(6 downto 0); -- se?al para el display de unidades de los centimos (siempre con un 0 por la naturaleza de las monedas insertadas)
        display1: in std_logic_vector(6 downto 0); -- se?al para el display de decenas de centimos
        display2: in std_logic_vector(6 downto 0); -- se?al para el display de centenas de centimos (los euros)
        BCD_OUT : out std_logic_vector(6 downto 0)
      );
    end component;

  --constant mux_channels : positive := 8;
  constant clock_period : time := 10 ns;
  constant delay        : time := 0.1 * clock_period;

  --Inputs
  signal sel    : std_logic_vector(2 downto 0);
  --signal bcd_in : bcd_vector(mux_channels - 1 downto 0);
  signal display0: std_logic_vector(6 downto 0); -- se?al para el display de unidades de los centimos (siempre con un 0 por la naturaleza de las monedas insertadas)
  signal display1: std_logic_vector(6 downto 0); -- se?al para el display de decenas de centimos
  signal display2: std_logic_vector(6 downto 0); -- se?al para el display de centenas de centimos (los euros)

  --Outputs
  signal bcd_out : std_logic_vector(6 downto 0);
 
begin

  -- Instantiate the Unit Under Test (UUT)
  uut: MUXER
    generic map (
      DIGITS  => 3
    )
    port map (
      SEL       => sel,
      display0  => display0,
      display1  => display1,
      display2  => display2,
      BCD_OUT => bcd_out
    );

  -- Stimulus process
  stim_proc: process
  begin
    display0 <= "0000000";
    display1 <= "0000001";
    display2 <= "0000010";

    sel(0) <= '0';
    sel(sel'high downto 1) <= (others => '1');    
    for i in 1 to 6 loop
      wait for clock_period;
      sel <= sel(sel'high - 1 downto 0) & sel(sel'high);
    end loop;

    wait for 0.25 * clock_period;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end;
