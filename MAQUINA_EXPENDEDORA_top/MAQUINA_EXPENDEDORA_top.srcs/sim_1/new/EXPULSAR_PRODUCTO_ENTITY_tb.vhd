library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity EXPULSAR_PRODUCTO_ENTITY_tb is
end;

architecture bench of EXPULSAR_PRODUCTO_ENTITY_tb is

  component EXPULSAR_PRODUCTO_ENTITY
      Generic (cantidad: integer:= 4
      );
      Port ( producto : in STD_LOGIC_VECTOR (cantidad-1 downto 0);
             expulsar_producto : in STD_LOGIC;
             producto_expulsado : out STD_LOGIC;
             sensor_producto: in STD_LOGIC;
             expulsar : out STD_LOGIC_VECTOR (cantidad-1 downto 0));
  end component;

  signal producto: STD_LOGIC_VECTOR (3 downto 0);
  signal expulsar_producto: STD_LOGIC;
  signal producto_expulsado: STD_LOGIC;
  signal sensor_producto: STD_LOGIC;
  signal expulsar: STD_LOGIC_VECTOR (3 downto 0);

begin

  uut: EXPULSAR_PRODUCTO_ENTITY generic map ( cantidad          =>  4)
                                   port map ( producto           => producto,
                                              expulsar_producto  => expulsar_producto,
                                              producto_expulsado => producto_expulsado,
                                              sensor_producto    => sensor_producto,
                                              expulsar           => expulsar );

  stimulus: process
  begin
  
    
	expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0000";
    wait for 5ns;
      

    -- Put test bench stimulus code here
    
	-- Prodcuto 1 ----------------------
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "1000";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '0';
    producto <= "1000";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '1';
    producto <= "1000";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '1';
    producto <= "1000";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "1000";
    wait for 5ns;
    
    -- Producto 2 ------------------
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0100";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '0';
    producto <= "0100";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '1';
    producto <= "0100";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '1';
    producto <= "0100";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0100";
    wait for 5ns;
    
    -- Prodcuto 3 ----------------------
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0010";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '0';
    producto <= "0010";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '1';
    producto <= "0010";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '1';
    producto <= "0010";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0010";
    wait for 5ns;
    
    -- Prodcuto 4 ----------------------
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0001";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '0';
    producto <= "0001";
    wait for 5ns;
    
    expulsar_producto <= '1';
    sensor_producto <= '1';
    producto <= "0001";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '1';
    producto <= "0001";
    wait for 5ns;
    
    expulsar_producto <= '0';
    sensor_producto <= '0';
    producto <= "0001";
    wait for 5ns;

    wait;
 end process;


end architecture;