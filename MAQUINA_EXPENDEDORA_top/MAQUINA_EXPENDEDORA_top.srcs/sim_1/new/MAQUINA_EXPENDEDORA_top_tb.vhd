-- Testbench created online at:
--   https://www.doulos.com/knowhow/perl/vhdl-testbench-creation-using-perl/
-- Copyright Doulos Ltd

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity MAQUINA_EXPENDEDORA_top_tb is
end;

architecture bench of MAQUINA_EXPENDEDORA_top_tb is

  component MAQUINA_EXPENDEDORA_top
    Port (
          prod: IN std_logic_vector (3 DOWNTO 0);
          CLK: IN std_logic ;
          DIEZ_cent: IN std_logic;
          VEINTE_cent: IN std_logic;
          CINCUENTA_cent: IN std_logic;
          UNO_euro: IN std_logic;
          SENSOR_producto: IN std_logic;
          SENSOR_monedas: IN std_logic;
          RESET_FSM: IN std_logic;
          display_segmentos: out std_logic_vector(7 downto 0);
          display_eleccion: out std_logic_vector(7 downto 0);
          expulsar: out std_logic_Vector(3 downto 0);
          monedas_fuera: out std_logic
          
          
    );
  end component;

  signal prod: std_logic_vector (3 DOWNTO 0);
  signal CLK: std_logic;
  signal DIEZ_cent: std_logic;
  signal VEINTE_cent: std_logic;
  signal CINCUENTA_cent: std_logic;
  signal UNO_euro: std_logic;
  signal SENSOR_producto: std_logic;
  signal SENSOR_monedas: std_logic;
  signal RESET_FSM: std_logic;
  signal display_segmentos: std_logic_vector(7 downto 0);
  signal display_eleccion: std_logic_vector(7 downto 0);
  signal expulsar: std_logic_Vector(3 downto 0);
  signal monedas_fuera: std_logic ;
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;
  
begin

  uut: MAQUINA_EXPENDEDORA_top port map ( prod              => prod,
                                          CLK               => CLK,
                                          DIEZ_cent         => DIEZ_cent,
                                          VEINTE_cent       => VEINTE_cent,
                                          CINCUENTA_cent    => CINCUENTA_cent,
                                          UNO_euro          => UNO_euro,
                                          SENSOR_producto   => SENSOR_producto,
                                          SENSOR_monedas    => SENSOR_monedas,
                                          RESET_FSM         => not RESET_FSM,
                                          display_segmentos => display_segmentos,
                                          display_eleccion  => display_eleccion,
                                          expulsar          => expulsar,
                                          monedas_fuera     => monedas_fuera );

  stimulus: process
  begin
  
    -- Put initialisation code here
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='1';
--          wait for (100ns);
          wait for 10*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (100ns);
          wait for 10*clock_period;

    -- Put test bench stimulus code here
--          prod <= "0000";
--          DIEZ_cent <='1';
--          VEINTE_cent <='0';
--          CINCUENTA_cent <='0';
--          UNO_euro <='0';
--          SENSOR_producto <= '0';
--          SENSOR_monedas <='0';
--          RESET_FSM <='0';
----          wait for (50ns);
--          wait for 5*clock_period;
          
          
--                    prod <= "0000";
--          DIEZ_cent <='0';
--          VEINTE_cent <='0';
--          CINCUENTA_cent <='0';
--          UNO_euro <='0';
--          SENSOR_producto <= '0';
--          SENSOR_monedas <='0';
--          RESET_FSM <='0';
----          wait for (50ns);
--          wait for 5*clock_period;
          
          
--                    prod <= "0000";
--          DIEZ_cent <='1';
--          VEINTE_cent <='0';
--          CINCUENTA_cent <='0';
--          UNO_euro <='0';
--          SENSOR_producto <= '0';
--          SENSOR_monedas <='0';
--          RESET_FSM <='0';
----          wait for (50ns);
--          wait for 5*clock_period;
                    
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='1';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
           prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='1';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='1';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='1';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "1000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (400ns);
          wait for 40*clock_period;
                    
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '1';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          
              -- Put initialisation code here
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='1';
--          wait for (100ns);
          wait for 10*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (100ns);
          wait for 10*clock_period;

    -- Put test bench stimulus code here
          prod <= "0000";
          DIEZ_cent <='1';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
                    
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='1';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
           prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='1';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='1';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='1';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "1000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (400ns);
          wait for 40*clock_period;
                    
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '1';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
          prod <= "0000";
          DIEZ_cent <='0';
          VEINTE_cent <='0';
          CINCUENTA_cent <='0';
          UNO_euro <='0';
          SENSOR_producto <= '0';
          SENSOR_monedas <='0';
          RESET_FSM <='0';
--          wait for (50ns);
          wait for 5*clock_period;
          
     --Parada reloj
    stop_the_clock <= true;
    wait;
  end process;

clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;