----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.12.2021 15:49:57
-- Design Name: 
-- Module Name: EXPULSAR_MONEDAS_ENTITY_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity EXPULSAR_MONEDAS_ENTITY_tb is
end;

architecture bench of EXPULSAR_MONEDAS_ENTITY_tb is

  component EXPULSAR_MONEDAS_ENTITY
      Port ( sensor_monedas : in STD_LOGIC;
             expulsar_monedas : in STD_LOGIC;
             no_hay_monedas: out STD_LOGIC;
             monedas_fuera : out STD_LOGIC);
  end component;

  signal sensor_monedas: STD_LOGIC;
  signal expulsar_monedas: STD_LOGIC;
  signal no_hay_monedas: STD_LOGIC;
  signal monedas_fuera: STD_LOGIC;

begin

  uut: EXPULSAR_MONEDAS_ENTITY port map ( sensor_monedas   => sensor_monedas,
                                          expulsar_monedas => expulsar_monedas,
                                          no_hay_monedas   => no_hay_monedas,
                                          monedas_fuera    => monedas_fuera );

  stimulus: process
  begin
  
    -- Put initialisation code here
	sensor_monedas <= '0';
    expulsar_monedas <= '0';
	wait for 5ns;
    -- Put test bench stimulus code here
    sensor_monedas <= '0';
    expulsar_monedas <= '1';
	wait for 5ns;
    
    sensor_monedas <= '1';
    expulsar_monedas <= '0';
	wait for 5ns;
    
    sensor_monedas <= '1';
    expulsar_monedas <= '1';
	wait for 5ns;

    wait;
  end process;


end;
